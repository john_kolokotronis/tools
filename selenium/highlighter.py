# The following script will open a web browser of your choice (Chrome, Firefox or Safari) and pause the session, thus
# allowing you to interact with the page under test both manually and via Selenium (except in Safari, where you have to
# use separate instances). Any arbitrary code specified before the breakpoint (code.interact), will be available for
# the life of your session, from the terminal where this script was launched.
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from os.path import expanduser
import code

home_folder = expanduser('~')

# Comment/un-comment the following sections as desired to launch your chosen browser:
# Chrome (Assumes ChromeDriver can be found in your <home>/selenium/drivers folder):
driver = webdriver.Chrome(executable_path=home_folder + '/selenium/drivers/chromedriver')

# Firefox (Assumes GeckoDriver can be found in your <home>/selenium/drivers folder)::
# driver = webdriver.Firefox(executable_path=home_folder + '/selenium/drivers/geckodriver')

# Safari:
# https://developer.apple.com/documentation/webkit/testing_with_webdriver_in_safari
# Safaridriver should already be present on your system (High Sierra+): /usr/bin/safaridriver
# Run `safaridriver --enable` once from the terminal, before first use. Requires authentication.
# Note that the first time you use SafariDriver, you may get superfluous browser windows opening which also do not
# get shut down by driver.quit(). Subsequent launches of SafariDriver don't seem to result in this issue.
# driver = webdriver.Safari()
# The Safari window driven by SafariDriver tends to be quite small by default, so let's resize it to something
# more reasonable:
# driver.set_window_size(1024, 768)


def highlight(element):
    driver = element._parent
    driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                          element,
                          "background: yellow; border: 2px solid red;")

# driver.get('')    # Set this to the URL you'd like the session to navigate to initially.

# You can also set a WebDriverWait as required:
# WebDriverWait(driver, <timeout>).until(lambda condition: <condition to evaluate>)

# Specify any arbitrary function/code you want available when the script pauses, here.

# Execution will pause here - note that you can have multiple such breakpoints throughout a larger script:
code.interact(local=dict(globals(), **locals()))

# Interact with the site under test here, either manually or via the terminal, driving the browser through Selenium.
# Any elements you want to visually identify can be accessed with the highlight function, after you've specified your
# identifiers:

# my_element = driver.find_element(By.CSS_SELECTOR, <some element identifier>)
# highlight(my_element)
# Simply refresh the page to clear the highlighted element.

# Use Ctrl + D to resume execution, quiting the open WebDriver session:
driver.quit()
