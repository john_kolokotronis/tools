# Open a terminal to ~/selenium and run:
#
# java -jar selenium-server-standalone-3.141.59.jar -role hub
#
# Open another terminal tab to ~/selenium and run:
#
# java -Dwebdriver.chrome.driver=drivers/chromedriver -Dwebdriver.gecko.driver=drivers/geckodriver -jar selenium-server-standalone-3.141.59.jar -role node
#
# You can then monitor your local Selenium Grid setup at:
#
# http://localhost:4444/grid/console
#
# The Internet Explorer 11 standalone Selenium instance is available at:
#
# http://192.168.110.72:4444/wd/hub
#
# The Hub/Node for the Selenium Grid should always be running before attempting to execute this script.

from collections import namedtuple
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
import os
from skimage.measure import compare_ssim
import imutils
import cv2
from PIL import Image, ImageDraw, ImageFont
import colorama
from colorama import Fore, Back, Style
import re
import pickle
import argparse
import time

parser = argparse.ArgumentParser()
parser.add_argument('-m', '--mode', help='run mode to use - baseline/comparison', required=True)
args = vars(parser.parse_args())
BASE_URL = 'https://cml.demo.bibliocommons.com/'

colorama.init()

def modify_style(element):
    driver = element._parent
    driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                          element,
                          "background: blue;")


def set_window_position_and_viewport(driver, width, height):
    x = 22
    y = 22

    window_size = driver.execute_script("""
        return [window.outerWidth - window.innerWidth + arguments[0],
          window.outerHeight - window.innerHeight + arguments[1]];
        """, width, height)
    if "safari" in driver.capabilities['browserName'].lower():
        driver.execute_script("window.moveTo(arguments[0], arguments[1]);", x, y)
        driver.execute_script("window.resizeTo(arguments[0], arguments[1]);", *window_size)
    else:
        if "internet explorer" in driver.capabilities['browserName']:
            pass
        else:
            driver.set_window_position(x, y)
        driver.set_window_size(*window_size)


def get_page_name(driver):
    if "v2/search" in driver.current_url:
        name = "search-results-page"
    elif "/user/login" in driver.current_url:
        name = "login-page"
    elif "/item/show/" in driver.current_url:
        name = "bib-page"
    elif "/v2/checkedout" in driver.current_url:
        name = "checked-out-page"
    elif "/v2/holds" in driver.current_url:
        name = "holds-page"
    elif "/v2/fines" in driver.current_url:
        name = "fines-page"
    elif "/v2/recentlyreturned" in driver.current_url:
        name = "recently-returned-page"
    elif "/shelves/for_later/" in driver.current_url:
        name = "for-later-shelf-page"
    elif "/shelves/in_progress" in driver.current_url:
        name = "in-progress-shelf-page"
    elif "/shelves/completed" in driver.current_url:
        name = "completed-shelf-page"
    elif "/lists/show/" in driver.current_url:
        name = "lists-page"
    elif "/explore/recent_arrivals" in driver.current_url:
        name = "recent-arrivals-page"
    elif "/list/share" in driver.current_url:
        name = "list-page"
    else:
        name = "home-page"
    return name


def take_screenshots(configurations, baseline=False):
    for configuration in configurations:
        if "chrome" in configuration.browser:
            options = webdriver.ChromeOptions()
            if configuration.width < 500:
                options.add_argument("headless")
            capabilities = options.to_capabilities()
        elif "firefox" in configuration.browser:
            options = webdriver.FirefoxOptions()
            capabilities = options.to_capabilities()
        elif "safari" in configuration.browser:
            # /usr/bin/safaridriver --enable
            capabilities = {'browserName': 'safari', 'platform': 'ANY'}
        # elif "internet explorer" in configuration.browser:
        #     capabilities = {'browserName': 'internet explorer', 'platform': 'ANY'}

        # if "internet explorer" in configuration.browser:
        #     driver = webdriver.Remote(desired_capabilities=capabilities,
        #                               command_executor='http://192.168.110.72:4444/wd/hub')
        # else:
        driver = webdriver.Remote(desired_capabilities=capabilities,
                                      command_executor='http://localhost:4444/wd/hub')

        # Login to the application before heading to desired page
        if configuration.login:
            login(driver, 'cml_6pl', '1234')
            WebDriverWait(driver, 10).until(lambda url_changed: "/user/login" not in driver.current_url)

        driver.get(configuration.target)
        version = driver.find_element(By.CLASS_NAME, 'product-version')
        _ = re.search(":\s*\w+", version.text)
        server = str(_.group()).replace(': ', '').replace(':', '')
        driver.execute_script("arguments[0].parentNode.removeChild(arguments[0])", version)
        if "chrome" in configuration.browser and configuration.width < 500:
            driver.set_window_size(configuration.width, configuration.height)
        else:
            set_window_position_and_viewport(driver, configuration.width, configuration.height)
        time.sleep(5)
        # WebDriverWait(driver, 15).until(lambda condition: driver.execute_script("return jQuery.active == 0"))
        page_name = get_page_name(driver)
        image_name = "{}-{}-{}x{}.png".format(page_name, configuration.browser, configuration.width,
                                              configuration.height)
        pickle_name = image_name.replace(".png", ".p")
        if "safari" in configuration.browser:
            browser_version = driver.capabilities['version']
        else:
            browser_version = driver.capabilities['browserVersion']
        metadata = {
            'server': server,
            'browser': driver.capabilities['browserName'].capitalize(),
            'browser_version': browser_version
        }
        if baseline:
            driver.save_screenshot("screenshots/baseline/" + image_name)
            with open("screenshots/baseline/" + pickle_name, 'wb') as fp:
                pickle.dump(metadata, fp, protocol=pickle.HIGHEST_PROTOCOL)
        else:
            # element = driver.find_element(By.CLASS_NAME, "heading-moderate")
            # modify_style(element)
            driver.save_screenshot("screenshots/current/" + image_name)
            with open("screenshots/current/" + pickle_name, 'wb') as fp:
                pickle.dump(metadata, fp, protocol=pickle.HIGHEST_PROTOCOL)
        driver.quit()


def compare_screenshots(image1, image2):
    file = os.path.basename(image1)

    # Load the two input images:
    imageA = cv2.imread(image1)
    imageB = cv2.imread(image2)

    # Convert the images to grayscale:
    grayA = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)
    grayB = cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)

    # Compute the Structural Similarity Index (SSIM) between the two
    # images, ensuring that the difference image is returned
    (score, diff) = compare_ssim(grayA, grayB, full=True)
    diff = (diff * 255).astype("uint8")

    # Threshold the difference image, followed by finding contours to
    # obtain the regions of the two input images that differ:
    thresh = cv2.threshold(diff, 0, 255,
                           cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)

    # Loop over the contours:
    for c in cnts:
        # Compute the bounding box of the contour and then draw the
        # bounding box on both input images to represent where the two
        # images differ:
        (x, y, w, h) = cv2.boundingRect(c)
        cv2.rectangle(imageA, (x, y), (x + w, y + h), (0, 0, 255), 2)
        cv2.rectangle(imageB, (x, y), (x + w, y + h), (0, 0, 255), 2)

    # Save the annotated images:
    cv2.imwrite("screenshots/diffs/diffA-" + file, imageA)
    cv2.imwrite("screenshots/diffs/diffB-" + file, imageB)
    time.sleep(5)

    if score < 1.0:
        baseline = Image.open(image1)
        comparison = Image.open(image2)
        baseline_diff = Image.open("screenshots/diffs/diffA-" + file)
        comparison_diff = Image.open("screenshots/diffs/diffB-" + file)

        width, height = baseline.size
        canvas_width = (width * 4) + 40
        canvas_height = height

        grid = Image.new("RGB", (canvas_width, canvas_height + 60), (70, 66, 127))

        grid.paste(baseline, (0, 50))
        grid.paste(comparison, (width + 10, 50))
        grid.paste(baseline_diff, ((width * 2) + 20, 50))
        grid.paste(comparison_diff, ((width * 3) + 30, 50))

        baseline_pickle_file = image1.replace(".png", ".p")
        with open(baseline_pickle_file, 'rb') as fp:
            baseline_metadata = pickle.load(fp)

        comparison_pickle_file = image2.replace(".png", ".p")
        with open(comparison_pickle_file, 'rb') as fp:
            comparison_metadata = pickle.load(fp)

        draw = ImageDraw.Draw(grid)
        font = ImageFont.truetype("arial.ttf", 16)
        draw.text((0, 0),
                  "{} {} - {} - {} * {}".format(baseline_metadata["browser"], baseline_metadata["browser_version"],
                                                baseline_metadata["server"], baseline.size[0], baseline.size[1]),
                  (255, 255, 255), font=font)
        draw.text((width + 10, 0),
                  "{} {} - {} - {} * {}".format(comparison_metadata["browser"], comparison_metadata["browser_version"],
                                                comparison_metadata["server"], comparison.size[0], comparison.size[1]),
                  (255, 255, 255), font=font)

        grid.save("screenshots/failures/" + file)

    return score


def delete_screenshots(directories):
    for dir in directories:
        for filename in os.listdir(dir):
            if filename.endswith(".png"):
                os.unlink(dir + "/" + filename)


def delete_pickles():
    for dir in ["screenshots/current/"]:  # "screenshots/baseline/"
        for filename in os.listdir(dir):
            if filename.endswith(".p"):
                os.unlink(dir + "/" + filename)


def login(driver, username, password):
    login_page = driver
    login_page.get(BASE_URL + "/user/login")
    login_page.find_element(By.CSS_SELECTOR, "[testid='field_username']").send_keys(username)
    login_page.find_element(By.CSS_SELECTOR, "[testid='field_userpin']").send_keys(password)
    login_page.find_element(By.CSS_SELECTOR, "[testid='button_login']").click()


Configuration = namedtuple("Configuration", "browser width height target login")
configurations = []

URLs = [
    "https://cml.demo.bibliocommons.com/",
    "https://cml.demo.bibliocommons.com/v2/search?query=a&searchType=smart",
    "https://cml.demo.bibliocommons.com/item/show/1740569105",
    "https://cml.demo.bibliocommons.com/user/login?destination=%2Fuser_dashboard",
    "https://cml.demo.bibliocommons.com/list/share/69429559_ottawareads/138169351_books_about_loved_books",
    "https://cml.demo.bibliocommons.com/v2/users/1367859527/shelves/completed",
    "https://cml.demo.bibliocommons.com/v2/users/1367859527/shelves/for_later",
    "https://cml.demo.bibliocommons.com/explore/recent_arrivals",
    "https://cml.demo.bibliocommons.com/v2/holds",
    "https://cml.demo.bibliocommons.com/v2/fines",
    "https://cml.demo.bibliocommons.com/v2/recentlyreturned",
    "https://cml.demo.bibliocommons.com/v2/checkedout"
]

for site in URLs:
    # ["chrome", "firefox", "safari", "internet explorer"]
    for browser in ["chrome", "firefox", "safari"]:
        if "checkedout" in site or "holds" in site or "fines" in site or "recentlyreturned" in site:
            configurations.append(Configuration(browser=browser, width=1380, height=780, target=site, login=True))  # 1380*780
            configurations.append(Configuration(browser=browser, width=990, height=780, target=site, login=True))  # 990*780
            configurations.append(Configuration(browser=browser, width=766, height=780, target=site, login=True))  # 766*780
            configurations.append(Configuration(browser=browser, width=380, height=667, target=site, login=True))  # 380*667
        else:
            configurations.append(Configuration(browser=browser, width=1380, height=780, target=site, login=False))  # 1380*780
            configurations.append(Configuration(browser=browser, width=990, height=780, target=site, login=False))  # 990*780
            configurations.append(Configuration(browser=browser, width=766, height=780, target=site, login=False))  # 766*780
            configurations.append(Configuration(browser=browser, width=380, height=667, target=site, login=False))  # 380*667

# Create our "screenshots" directories if they do not exist:
try:
    os.makedirs("screenshots/baseline")
    os.makedirs("screenshots/current")
    os.makedirs("screenshots/diffs")
    os.makedirs("screenshots/failures")
except FileExistsError:
    # Directories already exist:
    pass

if args['mode'].casefold() == "baseline":
    take_screenshots(configurations, baseline=True)
elif args['mode'].casefold() == "comparison":
    delete_screenshots(["screenshots/current", "screenshots/diffs", "screenshots/failures"])
    take_screenshots(configurations)
    for filename in os.listdir("screenshots/baseline"):
        if filename.endswith(".png"):
            base_image = "screenshots/baseline/" + filename
            comparison_image = "screenshots/current/" + filename
            try:
                score = compare_screenshots(base_image, comparison_image)
                if score == 1.0:
                    print('Screenshot: {:<35} SSIM: {:<30} 😄'.format(filename, Fore.GREEN + str(score) + Style.RESET_ALL))
                else:
                    print('Screenshot: {:<35} SSIM: {:<30} 💩'.format(filename, Fore.RED + str(score) + Style.RESET_ALL))
            except:
                try:
                    current_img = Image.open(comparison_image)
                    baseline_img = Image.open(base_image)
                    width_baseline_img, height_baseline_img = baseline_img.size
                    dimensions_baseline_img = "{}*{}".format(width_baseline_img, height_baseline_img)
                    width_current_img, height_current_img = current_img.size
                    dimensions_current_img = "{}*{}".format(width_current_img, height_current_img)
                    print('{:<35} Dimensions mismatched: {:<10} {:<10}'.format(Fore.RED + filename, dimensions_baseline_img, dimensions_current_img + Style.RESET_ALL))
                except FileNotFoundError:
                    print("{} does not exist and cannot be compared to baseline.".format(comparison_image))
    delete_pickles()
else:
    print("Mode argument should be: Baseline OR Comparison.")
