from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import StaleElementReferenceException
import os
from os.path import expanduser
import re
import sure


home_folder = expanduser('~')

driver = webdriver.Chrome(executable_path=home_folder + '/selenium/drivers/chromedriver')

if os.path.exists("output2.html"):
    os.remove("output2.html")

output_file = open("output2.html", "a")


def highlight(div):
    driver = div._parent
    driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                          div,
                          "background: yellow; border: 2px solid red;")


def css_properties(element):
    highlight(element)
    try:
        # Check if the DIV is actually visible:
        # if element.is_displayed():
        # Get the HTML of the "top" level CSS node:
        outer_HTML = element.get_attribute("outerHTML")
        top_level_node = re.match("<" + element.tag_name + ".*>", outer_HTML)
        return {
            "css": top_level_node[0],
            "text": element.text,
            "font": element.value_of_css_property("font-family"),
            "font_weight": element.value_of_css_property("font-weight"),
            "font_size": element.value_of_css_property("font-size"),
            "line_height": element.value_of_css_property("line-height"),
            "separator": 150 * "-"
        }
    except StaleElementReferenceException:
        raise StaleElementReferenceException


def get_giant_size_heading(URL, webelement, fontfamily, fontsize, fontweight, lineheight):
    for element in webelement.find_elements(By.CSS_SELECTOR, ".o-heading--giant"):
        found_properties = css_properties(element)
        lineheight_value = found_properties['line_height']
        temp = lineheight_value[0:-2]
        float_temp = float(temp)
        exact_lineheight = round(float_temp)
        print(found_properties['text'])
        # print(found_properties['css'])
        try:
            found_properties['font'].should.equal(fontfamily)
            found_properties['font_size'].should.equal(fontsize)
            found_properties['font_weight'].should.equal(fontweight)
            str(exact_lineheight).should.equal(lineheight)
        except AssertionError:
            print("Giant size heading error")
        if found_properties['font_size'] != fontsize:
            output_file.write(URL + ": expected: " + fontsize + ' actual: ' + found_properties['font_size'] + "\n \n")
        if found_properties['font_weight'] != fontweight:
            output_file.write(URL + ": expected: " + fontweight + ' actual: ' + found_properties['font_weight'] + "\n \n")
        if str(exact_lineheight) != lineheight:
            output_file.write(URL + ": expected: " + lineheight + ' actual: ' + str(exact_lineheight) + "\n \n")
        if found_properties['font'] != fontfamily:
            output_file.write(URL + ": expected: " + fontfamily + ' actual: ' + found_properties['font'] + "\n \n")


def get_large_size_heading(URL, webelement, fontfamily, fontsize, fontweight, lineheight):
    for element in webelement.find_elements(By.CSS_SELECTOR, ".o-heading--large"):
        found_properties = css_properties(element)
        lineheight_value = found_properties['line_height']
        temp = lineheight_value[0:-2]
        float_temp = float(temp)
        exact_lineheight = round(float_temp)
        print(found_properties['text'])
        # print(found_properties['css'])
        try:
            found_properties['font'].should.equal(fontfamily)
            found_properties['font_size'].should.equal(fontsize)
            found_properties['font_weight'].should.equal(fontweight)
            str(exact_lineheight).should.equal(lineheight)
        except AssertionError:
            print("Large size heading error")
        if found_properties['font_size'] != fontsize:
            output_file.write(URL + ": expected: " + fontsize + ' actual: ' + found_properties['font_size'] + "\n \n")
        if found_properties['font_weight'] != fontweight:
            output_file.write(URL + ": expected: " + fontweight + ' actual: ' + found_properties['font_weight'] + "\n \n")
        if str(exact_lineheight) != lineheight:
            output_file.write(URL + ": expected: " + lineheight + ' actual: ' + str(exact_lineheight) + "\n \n")
        if found_properties['font'] != fontfamily:
           output_file.write(URL + ": expected: " + fontfamily + ' actual: ' + found_properties['font'] + "\n \n")


def get_moderate_size_heading(URL, webelement, fontfamily, fontsize, fontweight, lineheight):
    for element in webelement.find_elements(By.CSS_SELECTOR, ".o-heading--moderate"):
        found_properties = css_properties(element)
        lineheight_value = found_properties['line_height']
        temp = lineheight_value[0:-2]
        float_temp = float(temp)
        exact_lineheight = round(float_temp)
        print(found_properties['text'])
        # print(found_properties['css'])
        try:
            found_properties['font'].should.equal(fontfamily)
            found_properties['font_size'].should.equal(fontsize)
            found_properties['font_weight'].should.equal(fontweight)
            str(exact_lineheight).should.equal(lineheight)
        except AssertionError:
            print("Moderate size heading error")
        if found_properties['font_size'] != fontsize:
            output_file.write(URL + ": expected: " + fontsize + ' actual: ' + found_properties['font_size'] + "\n \n")
        if found_properties['font_weight'] != fontweight:
            output_file.write(URL + ": expected: " + fontweight + ' actual: ' + found_properties['font_weight'] + "\n \n")
        if str(exact_lineheight) != lineheight:
            output_file.write(URL + ": expected: " + lineheight + ' actual: ' + str(exact_lineheight) + "\n \n")
        if found_properties['font'] != fontfamily:
           output_file.write(URL + ": expected: " + fontfamily + ' actual: ' + found_properties['font'] + "\n \n")


def get_medium_size_heading(URL, webelement, fontfamily, fontsize, fontweight, lineheight):
    for element in webelement.find_elements(By.CSS_SELECTOR, ".o-heading--medium"):
        found_properties = css_properties(element)
        lineheight_value = found_properties['line_height']
        temp = lineheight_value[0:-2]
        float_temp = float(temp)
        exact_lineheight = round(float_temp)
        print(found_properties['text'])
        # print(found_properties['css'])
        try:
            found_properties['font'].should.equal(fontfamily)
            found_properties['font_size'].should.equal(fontsize)
            found_properties['font_weight'].should.equal(fontweight)
            str(exact_lineheight).should.equal(lineheight)
        except AssertionError:
            print("Medium size heading error")
        if found_properties['font_size'] != fontsize:
            output_file.write(URL + ": expected: " + fontsize + ' actual: ' + found_properties['font_size'] + "\n \n")
        if found_properties['font_weight'] != fontweight:
            output_file.write(
                URL + ": expected: " + fontweight + ' actual: ' + found_properties['font_weight'] + "\n \n")
        if str(exact_lineheight) != lineheight:
            output_file.write(URL + ": expected: " + lineheight + ' actual: ' + str(exact_lineheight) + "\n \n")
        if found_properties['font'] != fontfamily:
            output_file.write(URL + ": expected: " + fontfamily + ' actual: ' + found_properties['font'] + "\n \n")


def get_modest_size_heading(URL, webelement, fontfamily, fontsize, fontweight, lineheight):
    for element in webelement.find_elements(By.CSS_SELECTOR, ".o-heading--modest"):
        found_properties = css_properties(element)
        lineheight_value = found_properties['line_height']
        temp = lineheight_value[0:-2]
        float_temp = float(temp)
        exact_lineheight = round(float_temp)
        print(found_properties['text'])
        # print(found_properties['css'])
        try:
            found_properties['font'].should.equal(fontfamily)
            found_properties['font_size'].should.equal(fontsize)
            found_properties['font_weight'].should.equal(fontweight)
            str(exact_lineheight).should.equal(lineheight)
        except AssertionError:
            print("Modest size heading error")
        if found_properties['font_size'] != fontsize:
            output_file.write(URL + ": expected: " + fontsize + ' actual: ' + found_properties['font_size'] + "\n \n")
        if found_properties['font_weight'] != fontweight:
            output_file.write(URL + ": expected: " + fontweight + ' actual: ' + found_properties['font_weight'] + "\n \n")
        if str(exact_lineheight) != lineheight:
            output_file.write(URL + ": expected: " + lineheight + ' actual: ' + str(exact_lineheight) + "\n \n")
        if found_properties['font'] != fontfamily:
           output_file.write(URL + ": expected: " + fontfamily + ' actual: ' + found_properties['font'] + "\n \n")


def get_small_size_heading(URL, webelement, fontfamily, fontsize, fontweight, lineheight):
    for element in webelement.find_elements(By.CSS_SELECTOR, ".o-heading--small"):
        found_properties = css_properties(element)
        lineheight_value = found_properties['line_height']
        temp = lineheight_value[0:-2]
        float_temp = float(temp)
        exact_lineheight = round(float_temp)
        print(found_properties['text'])
        # print(found_properties['css'])
        try:
            found_properties['font'].should.equal(fontfamily)
            found_properties['font_size'].should.equal(fontsize)
            found_properties['font_weight'].should.equal(fontweight)
            str(exact_lineheight).should.equal(lineheight)
        except AssertionError:
            print("Small size heading error")
        if found_properties['font_size'] != fontsize:
            output_file.write(URL + ": expected: " + fontsize + ' actual: ' + found_properties['font_size'] + "\n \n")
        if found_properties['font_weight'] != fontweight:
            output_file.write(
                URL + ": expected: " + fontweight + ' actual: ' + found_properties['font_weight'] + "\n \n")
        if str(exact_lineheight) != lineheight:
            output_file.write(URL + ": expected: " + lineheight + ' actual: ' + str(exact_lineheight) + "\n \n")
        if found_properties['font'] != fontfamily:
            output_file.write(URL + ": expected: " + fontfamily + ' actual: ' + found_properties['font'] + "\n \n")


# Page Builder Headings
def get_page_builder_heading(URL, webelement, tag, fontfamily, fontsize, fontweight, lineheight):
    for element in webelement.find_elements(By.TAG_NAME, tag):
        found_properties = css_properties(element)
        lineheight_value = found_properties['line_height']
        temp = lineheight_value[0:-2]
        float_temp = float(temp)
        exact_lineheight = round(float_temp)
        print(found_properties['text'])
        # print(found_properties['css'])
        try:
            found_properties['font'].should.equal(fontfamily)
            found_properties['font_size'].should.equal(fontsize)
            found_properties['font_weight'].should.equal(fontweight)
            str(exact_lineheight).should.equal(lineheight)
        except AssertionError:
            print("Page Builder heading error")
        if found_properties['font_size'] != fontsize:
            output_file.write(URL + ": expected: " + fontsize + ' actual: ' + found_properties['font_size'] + "\n \n")
        if found_properties['font_weight'] != fontweight:
            output_file.write(URL + ": expected: " + fontweight + ' actual: ' + found_properties['font_weight'] + "\n \n")
        if str(exact_lineheight) != lineheight:
            output_file.write(URL + ": expected: " + lineheight + ' actual: ' + str(exact_lineheight) + "\n \n")
        if found_properties['font'] != fontfamily:
           output_file.write(URL + ": expected: " + fontfamily + ' actual: ' + found_properties['font'] + "\n \n")


# Footer Headings
def get_footer_heading(URL, webelement, fontfamily, fontsize, fontweight):
    for element in webelement.find_elements_by_tag_name("h3"):
        found_properties = css_properties(element)
        try:
            found_properties['font'].should.equal(fontfamily)
            found_properties['font_size'].should.equal(fontsize)
            found_properties['font_weight'].should.equal(fontweight)
        except AssertionError:
            print("Footer error")
        if found_properties['font_size'] != fontsize:
            output_file.write(URL + ": expected: " + fontsize + ' actual: ' + found_properties['font_size'] + "\n \n")
        if found_properties['font_weight'] != fontweight:
            output_file.write(URL + ": expected: " + fontweight + ' actual: ' + found_properties['font_weight'] + "\n \n")
        if found_properties['font'] != fontfamily:
            output_file.write(URL + ": expected: " + fontfamily + ' actual: ' + found_properties['font'] + "\n \n")


# Archives Collections title Headings
def get_archives_collections_heading(URL, webelement, fontfamily, fontsize, fontweight):
    for element in webelement.find_elements_by_css_selector("h3.entry-header"):
        found_properties = css_properties(element)
        try:
            found_properties['font'].should.equal(fontfamily)
            found_properties['font_size'].should.equal(fontsize)
            found_properties['font_weight'].should.equal(fontweight)
            print(found_properties['text'])
        except AssertionError:
            print("Error")
        if found_properties['font_size'] != fontsize:
            output_file.write(URL + ": expected: " + fontsize + ' actual: ' + found_properties['font_size'] + "\n \n")
        if found_properties['font_weight'] != fontweight:
            output_file.write(URL + ": expected: " + fontweight + ' actual: ' + found_properties['font_weight'] + "\n \n")
        if found_properties['font'] != fontfamily:
            output_file.write(URL + ": expected: " + fontfamily + ' actual: ' + found_properties['font'] + "\n \n")


# Press Releases
def get_press_releases_heading(URL, webelement, fontfamily, fontsize, fontweight):
    for element in webelement.find_elements_by_css_selector("h2.c-press-list__month-year"):
        found_properties = css_properties(element)
        try:
            found_properties['font'].should.equal(fontfamily)
            found_properties['font_size'].should.equal(fontsize)
            found_properties['font_weight'].should.equal(fontweight)
            print(found_properties['text'])
        except AssertionError:
            print("Error")
        if found_properties['font_size'] != fontsize:
            output_file.write(URL + ": expected: " + fontsize + ' actual: ' + found_properties['font_size'] + "\n \n")
        if found_properties['font_weight'] != fontweight:
            output_file.write(URL + ": expected: " + fontweight + ' actual: ' + found_properties['font_weight'] + "\n \n")
        if found_properties['font'] != fontfamily:
            output_file.write(URL + ": expected: " + fontfamily + ' actual: ' + found_properties['font'] + "\n \n")


# Headings in description of Blog/News post
def get_post_description_heading(URL, webelement, tag, fontfamily, fontsize, fontweight, lineheight):
    for element in webelement.find_elements(By.TAG_NAME, tag):
        found_properties = css_properties(element)
        lineheight_value = found_properties['line_height']
        temp = lineheight_value[0:-2]
        float_temp = float(temp)
        exact_lineheight = round(float_temp)
        print(found_properties['text'])
        # print(found_properties['css'])
        try:
            found_properties['font'].should.equal(fontfamily)
            found_properties['font_size'].should.equal(fontsize)
            found_properties['font_weight'].should.equal(fontweight)
            str(exact_lineheight).should.equal(lineheight)
        except AssertionError:
            print("Blog Description heading error")
        if found_properties['font_size'] != fontsize:
            output_file.write(URL + ": expected: " + fontsize + ' actual: ' + found_properties['font_size'] + "\n \n")
        if found_properties['font_weight'] != fontweight:
            output_file.write(URL + ": expected: " + fontweight + ' actual: ' + found_properties['font_weight'] + "\n \n")
        if str(exact_lineheight) != lineheight:
            output_file.write(URL + ": expected: " + lineheight + ' actual: ' + str(exact_lineheight) + "\n \n")
        if found_properties['font'] != fontfamily:
           output_file.write(URL + ": expected: " + fontfamily + ' actual: ' + found_properties['font'] + "\n \n")

# Headings in description of FAQs
def get_faqs_answers_heading(URL, webelement, tag, fontfamily, fontsize, fontweight, lineheight):
    for element in webelement.find_elements(By.TAG_NAME, tag):
        found_properties = css_properties(element)
        lineheight_value = found_properties['line_height']
        temp = lineheight_value[0:-2]
        float_temp = float(temp)
        exact_lineheight = round(float_temp)
        print(found_properties['text'])
        # print(found_properties['css'])
        try:
            found_properties['font'].should.equal(fontfamily)
            found_properties['font_size'].should.equal(fontsize)
            found_properties['font_weight'].should.equal(fontweight)
            str(exact_lineheight).should.equal(lineheight)
        except AssertionError:
            print("FAQ answers heading error")
        if found_properties['font_size'] != fontsize:
            output_file.write(URL + ": expected: " + fontsize + ' actual: ' + found_properties['font_size'] + "\n \n")
        if found_properties['font_weight'] != fontweight:
            output_file.write(URL + ": expected: " + fontweight + ' actual: ' + found_properties['font_weight'] + "\n \n")
        if str(exact_lineheight) != lineheight:
            output_file.write(URL + ": expected: " + lineheight + ' actual: ' + str(exact_lineheight) + "\n \n")
        if found_properties['font'] != fontfamily:
           output_file.write(URL + ": expected: " + fontfamily + ' actual: ' + found_properties['font'] + "\n \n")


def get_banner_title_heading(URL, webelement, fontfamily, fontsize, fontweight, lineheight):
    for element in webelement.find_elements_by_css_selector(".o-banner__text > h2.o-banner__title"):
        found_properties = css_properties(element)
        lineheight_value = found_properties['line_height']
        temp = lineheight_value[0:-2]
        float_temp = float(temp)
        exact_lineheight = round(float_temp)
        try:
            found_properties['font'].should.equal(fontfamily)
            found_properties['font_size'].should.equal(fontsize)
            found_properties['font_weight'].should.equal(fontweight)
            str(exact_lineheight).should.equal(lineheight)
            print(found_properties['text'])
        except AssertionError:
            print("Banner title heading error")
        if found_properties['font_size'] != fontsize:
            output_file.write(URL + ": expected: " + fontsize + ' actual: ' + found_properties['font_size'] + "\n \n")
        if found_properties['font_weight'] != fontweight:
            output_file.write(URL + ": expected: " + fontweight + ' actual: ' + found_properties['font_weight'] + "\n \n")
        if found_properties['font'] != fontfamily:
            output_file.write(URL + ": expected: " + fontfamily + ' actual: ' + found_properties['font'] + "\n \n")
        if str(exact_lineheight) != lineheight:
            output_file.write(URL + ": expected: " + lineheight + ' actual: ' + str(exact_lineheight) + "\n \n")


URLS = ["https://chipublib-demo.bibliocms.com/", "https://chipublib-demo.bibliocms.com/news/",
        "https://chipublib-demo.bibliocms.com/blogs/","https://chipublib-demo.bibliocms.com/locations/34/",
        "https://chipublib-demo.bibliocms.com/explore/", "https://chipublib-demo.bibliocms.com/archives-collections/",
        "https://chicago-stage.bibliocms.com/page-builder-headings/", "https://chipublib-demo.bibliocms.com/press-releases/",
        "https://chipublib-demo.bibliocms.com/blogs/post/rough-tumble-coming-of-age-films/",
        "https://chipublib-demo.bibliocms.com/news/youmedia-block-party-lineup/", "https://chipublib-demo.bibliocms.com/online-resources/",
        "https://chipublib-demo.bibliocms.com/faq/library-cards/", "https://chipublib-demo.bibliocms.com/ask-a-librarian/"]


# All headings that follow design system
for URL in URLS:
    driver.get(URL)
    webelement = driver.find_element_by_css_selector("div.container_12")  # excluding header and footer
    get_giant_size_heading(URL, webelement, "Montserrat, Georgia, serif", "40px", "700", "48")
    get_large_size_heading(URL, webelement, "Montserrat, Georgia, serif", "32px", "700", "40")
    get_moderate_size_heading(URL, webelement, "Montserrat, Georgia, serif", "24px", "700", "32")
    get_medium_size_heading(URL, webelement, "Montserrat, Georgia, serif", "20px", "700", "28")
    get_modest_size_heading(URL, webelement, "Montserrat, Georgia, serif", "16px", "700", "22")
    get_small_size_heading(URL, webelement, "Montserrat, Georgia, serif", "14px", "700", "20")

# Headings in FAQs answers
driver.get(URLS[11])
webelement = driver.find_element_by_css_selector("#content")
get_faqs_answers_heading(URLS[11], webelement, "h1", "Montserrat, Georgia, serif", "40px", "700", "48")
get_faqs_answers_heading(URLS[11], webelement, "h2", "Montserrat, Georgia, serif", "32px", "700", "40")
get_faqs_answers_heading(URLS[11], webelement, "h3", "Montserrat, Georgia, serif", "24px", "700", "32")
get_faqs_answers_heading(URLS[11], webelement, "h4", "Montserrat, Georgia, serif", "20px", "700", "28")
get_faqs_answers_heading(URLS[11], webelement, "h5", "Montserrat, Georgia, serif", "16px", "700", "22")
get_faqs_answers_heading(URLS[11], webelement, "h6", "Montserrat, Georgia, serif", "14px", "700", "20")


# Headings in News/Post/Forms Description
driver.get(URLS[8])  # use appropriate URL for News/Blogs/Forms
webelement = driver.find_element_by_css_selector(".entry-content")
get_post_description_heading(URLS[8], webelement, "h1", "Montserrat, Georgia, serif", "40px", "700", "48")
get_post_description_heading(URLS[8], webelement, "h2", "Montserrat, Georgia, serif", "32px", "700", "40")
get_post_description_heading(URLS[8], webelement, "h3", "Montserrat, Georgia, serif", "24px", "700", "32")
get_post_description_heading(URLS[8], webelement, "h4", "Montserrat, Georgia, serif", "20px", "700", "28")
get_post_description_heading(URLS[8], webelement, "h5", "Montserrat, Georgia, serif", "16px", "700", "22")
get_post_description_heading(URLS[8], webelement, "h6", "Montserrat, Georgia, serif", "14px", "700", "20")

# Headings, Accordion, Tabs, Text Editor modules of Page Builder
driver.get(URLS[6])
webelement = driver.find_element_by_class_name("fl-row-content-wrap")  # excluding header and footer
get_page_builder_heading(URLS[6], webelement, "h1", "Montserrat, Georgia, serif", "40px", "700", "48")
get_page_builder_heading(URLS[6], webelement, "h2", "Montserrat, Georgia, serif", "32px", "700", "40")
get_page_builder_heading(URLS[6], webelement, "h3", "Montserrat, Georgia, serif", "24px", "700", "32")
get_page_builder_heading(URLS[6], webelement, "h4", "Montserrat, Georgia, serif", "20px", "700", "28")
get_page_builder_heading(URLS[6], webelement, "h5", "Montserrat, Georgia, serif", "16px", "700", "22")
get_page_builder_heading(URLS[6], webelement, "h6", "Montserrat, Georgia, serif", "14px", "700", "20")

# Footer section
driver.get(URLS[0])
webelement = driver.find_element_by_class_name("footer_container_12") # excluding header and footer
get_footer_heading(URLS[0], webelement, "Montserrat, Georgia, serif", "16px", "700")

# Archives Collection Headings
driver.get(URLS[5])
webelement = driver.find_element_by_css_selector("div.container_12")
get_archives_collections_heading(URLS[5], webelement, "Montserrat, Georgia, serif", "24px", "700")

# Press Release Headings
driver.get(URLS[7])
webelement = driver.find_element_by_css_selector("div.container_12")
get_press_releases_heading(URLS[7], webelement, "Montserrat, Georgia, serif", "16px", "400")

# Banner title Heading
driver.get(URLS[0])
webelement = driver.find_element_by_css_selector("div.container_12")
get_banner_title_heading(URLS[0], webelement, "Montserrat, Georgia, serif", "14px", "700", "18")

driver.quit()

output_file.close()
