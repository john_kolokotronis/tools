# The following script allows a BC QA engineer to output a list of last updated dates for a given test suite, as a way
# of quickly checking when various test cases were last updated, from one central location. Oddly enough, the TestRail
# API has no support for pulling case history, even though the feature has been requested for years at this point:
# https://discuss.gurock.com/t/test-case-history-via-the-api/2975/13
# The script uses the TestRail API to get the list of case IDs for a given suite, then relies on Selenium, driving
# Chrome (Headless) to actually pull the historical data and output it to the command line. It is assumed ChromeDriver
# can be found in your home folder, /selenium/drivers.

from testrail import APIClient
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from os.path import expanduser

home_folder = expanduser('~')

TESTRAIL_URL = 'https://bibliocommons.testrail.com' 
PROJECT_ID = '15'
SUITE_ID = '669'

client = APIClient(TESTRAIL_URL)
client.user = 'a.tester@bibliocommons.com'
client.password = ''

options = webdriver.ChromeOptions()
options.add_argument("window-size=1920,1080")
options.add_argument("headless")
driver = webdriver.Chrome(executable_path=home_folder + '/selenium/drivers/chromedriver', options=options)
driver.get(TESTRAIL_URL + '/index.php?/auth/login')

driver.find_element(By.ID, 'name').send_keys(client.user)
driver.find_element(By.ID, 'password').send_keys(client.password)
driver.find_element(By.ID, 'button_primary').click()
WebDriverWait(driver, 10).until(lambda condition: 'dashboard' in driver.current_url)

cases = client.send_get('get_cases/{}&suite_id={}'.format(PROJECT_ID, SUITE_ID))
for case in cases:
    history_page = TESTRAIL_URL + '/index.php?/cases/history/' + str(case['id'])
    driver.get(history_page)

    change_dates = driver.find_element(By.ID, 'history').find_elements(By.CSS_SELECTOR, 'h3')
    print("{} - Last Updated: {}".format(history_page, change_dates[0].text))

driver.quit()
